# README #

This is a little code sample of mine.
The problem that i solved is this one https://paradox.kattis.com/problems/paradoxpath
I did it using A* as you can probably guess from the name of this repo.
But according to kattis my code returns a wrong answer. There is probably some kind of worst case scenario for pathfinding algorithms that i cannot imagine and i have no idea what i can do to counteract it. 
I included some unit tests to show that my solution is not straight up broken, but apparently its not perfect.

### What is this repository for? ###

To show that i know how to code and understand language mechanisms. People want code samples? Here is my code sample.
I tried to show here that i have an over average understanding of programming (when you consider other almost-bachelors of science), probably some stuff is suboptimal or there is better way of doing it, but i wouldn't know that. 
Programming is my passion and i enjoy all those endless possibilities of solving problems but i lack experience so if you think something is done better in a diffrent way feel free to contact me.


### What is where? ###

I tried to do some comments that will hopefully help you, dear reader understanding what i did and why i did it and that im not a complete idiot. There is also a really nice makefile in the /tests/ directory that finds all the source files and compiles them.
The unit tests are done using boost.