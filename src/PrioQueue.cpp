//
// Created by wysocki on 19.5.2016.
//

#include "MultithreadPrioQueue.h"

void MultithreadPrioQueue::push(Field *value) {
	std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>>::push(value);
//	for (int i = 0; i < c.size(); i++) {
//		std::cout << "Element " << i << " (" << c[i]->getX() << ", " << c[i]->getY() << ") Heur: " <<  c[i]->getHeur() << "\n";
//	}
//	Field * returnField = std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>>::top();
//	std::cout << "top is " << returnField->getHeur() << std::endl << std::endl;
}
Field * MultithreadPrioQueue::Pop() {
	Field * returnField = std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>>::top();
	std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>>::pop();
	return returnField;
}
bool MultithreadPrioQueue::isEmpty() {
	return std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>>::empty();
}
MultithreadPrioQueue::PrioQueue (std::function<bool(Field *, Field *)> compare) :
	std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>>(compare)
{

};