//
// Created by Wysocki on 18/05/2016.
//

#include "Field.h"
#include "LabData.hpp"
#include "MultithreadPrioQueue.h"
#include <cmath>
#include <iostream>

//i check its neighbours
Field *Field::validateNotDone(std::vector<Field> * fields) {
	int X = 0;
	int Y = 0;
	int R = 1;
	Field * neighbour;
	int directions[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
	for(int i = 0; i < 4; i++) {\
		X = directions[i][0];
		Y = directions[i][1];
		X += this->X;
		Y += this->Y;
		if((X >= 0 && X < labirynth->xSize) && (Y >= 0 && Y < labirynth->ySize)) {
			neighbour = &fields->at(X + (Y * labirynth->xSize));
			neighbour->ValidateNeighbour(this, X, Y);
		}
	}

	validateAsNeighbour = &Field::validateAsNeighbourNotPassable;
	return nullptr;
}

//i found the target so i return it
Field *Field::validateDone(std::vector<Field> * fields) {
	return this;
}

void Field::validateAsNeighbourFirst(Field * potentialPrev, int x, int y) {
	X = x;
	Y = y;
	if(labirynth->pMap[x + (labirynth->xSize * y)] == 1) {
		validateAsNeighbour = &Field::validateAsNeighbourPassable;
		validateAsNeighbourPassable(potentialPrev, x, y);
	} else {
		validateAsNeighbour = &Field::validateAsNeighbourNotPassable;
		validateAsNeighbourNotPassable(potentialPrev, x, y);
	}
}

void Field::validateAsNeighbourPassable(Field * potentialPrev, int x, int y) {
	int potentialDistance = potentialPrev->distance + 1;
	if(potentialDistance >= distance) return;

	prev = potentialPrev;
	distance = potentialDistance;
	heur = distance + getDistanceToTarget();
	//heur *= (1 + 1/1000);

	(this->*Field::afterProcess)();
}

void Field::validateAsNeighbourNotPassable(Field * potentialPrev, int x, int y) {
	return;
}

double Field::getDistanceToTarget() {
	double xAxis = abs(labirynth->xTarget - X);
	double yAxis = abs(labirynth->yTarget - Y);
	double result = sqrt(pow(xAxis, 2) + pow(yAxis, 2));
	return result;
}

void Field::notAddedAfterProcess() {
	labirynth->prioQueue->push(this);
	afterProcess = &Field::addedAfterProcess;
}

void Field::addedAfterProcess() {
	return;
}

void Field::SetAsStart(int x, int y) {
	X = x;
	Y = y;
	heur = 0;
	distance = 0;
}

void Field::SetAsTarget(int x, int y) {
	X = x;
	Y = y;
	validate = &Field::validateDone;
}
int Field::getNumber() {
	return X + (Y * labirynth->xSize);
}