//
// Created by Wysocki on 18/05/2016.
//

#ifndef ASTAR_MULTITHREADPRIOQUEUE_H
#define ASTAR_MULTITHREADPRIOQUEUE_H

#include <mutex>
#include <vector>
#include <queue>
#include <thread>
#include <functional>
#include <set>
#include <iostream>
#include <chrono>

class Field;

#include "Field.h"

//Did a priority queue where i keep fields to check with an easier interface to use (just 3 methods)
//It was once meant to be a multithreaded solution but for now i settled for a single threaded one, because its simpler to explain
class MultithreadPrioQueue : private std::priority_queue<Field*, std::vector<Field*>, std::function<bool(Field*, Field*)>> {
private:
public:
    void push(Field *value);

    Field * Pop();

	bool isEmpty();

    MultithreadPrioQueue (std::function<bool(Field *, Field *)> compare);
};


#endif //ASTAR_MULTITHREADPRIOQUEUE_H
