#include <iostream>
#include <mutex>
#include <vector>
#include <queue>
#include <thread>
#include <functional>
#include <set>
#include <chrono>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <algorithm>
#include <unistd.h>

#include "paradoxpath.hpp"
#include "MultithreadPrioQueue.h"

MultithreadPrioQueue CreateQueue();

using namespace std;

int FindPath(const int nStartX, const int nStartY,
             const int nTargetX, const int nTargetY,
             const unsigned char *pMap, const int nMapWidth, const int nMapHeight,
             int *pOutBuffer, const int nOutBufferSize) {

	int returnDistance = -1;

	MultithreadPrioQueue uncheckedPrioQueue = CreateQueue();

	LabData labirynth(pMap, nMapWidth, nMapHeight, nTargetX, nTargetY, &uncheckedPrioQueue);

	Field defaultField(&labirynth);

	vector <Field> Fields(nMapHeight * nMapWidth, defaultField);

	Fields[nStartX + (nStartY * nMapWidth)].SetAsStart(nStartX, nStartY);
	Fields[nTargetX + (nTargetY * nMapWidth)].SetAsTarget(nTargetX, nTargetY);

	uncheckedPrioQueue.push(&Fields[nStartX + (nStartY * nMapWidth)]);

	Field * currentField = nullptr;
	Field * found = nullptr;
	do {
		currentField = uncheckedPrioQueue.Pop();

		if(currentField) {
			found = currentField->Validate(&Fields);
			if(found != nullptr) break;
		}
	} while (!uncheckedPrioQueue.isEmpty());

	if(Fields[nTargetX + (nTargetY * nMapWidth)].prevField() != nullptr) {
		returnDistance = Fields[nTargetX + (nTargetY * nMapWidth)].getDist();
		generateRoute(&Fields[nTargetX + (nTargetY * nMapWidth)], pOutBuffer, nOutBufferSize);
	}

	return returnDistance;
}

MultithreadPrioQueue CreateQueue() {
	auto priority = [&](Field *left, Field *right) { return (left->getHeur()) >= (right->getHeur()); };
	return PrioQueue(priority);
}

void generateRoute(Field * target, int *pOutBuffer, const int nOutBufferSize) {
	vector<int> route;
	Field * current = target;
	do {
		route.push_back(current->getNumber());
		current = current->prevField();
	} while (current != nullptr);
	std::reverse(route.begin(), route.end());
	route.erase(route.begin());
	int size = 0;
	if(nOutBufferSize >= route.size()) {
		size = route.size();
	} else {
		size = nOutBufferSize;
	}
	for(int i = 0; i < size ; i++) {
		pOutBuffer[i] = route.at(i);
	}
}