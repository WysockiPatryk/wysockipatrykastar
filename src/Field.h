//
// Created by Wysocki on 18/05/2016.
//

#ifndef ASTAR_FIELD_H
#define ASTAR_FIELD_H

#include <mutex>
#include <iostream>
#include <vector>
#include <cmath>
#include <limits>

class LabData;


//Class for every field in the maze, hence the name
class Field {
	//Things needed for A* algorithm
	int distance;
	double heur;
	Field * prev;
	//Location in the maze, 0-based
	int X;
	int Y;
	//Structure with maze data
	LabData * labirynth;

	//Functional programming
	//Instead of doing a few bools and if checks i simply alter the state by changing the function reacting to the event
	//Maybe i can just call it a strategy, but they are not objects so im not sure if it applies

	//Pointer to function to call when field is being processed as a main field, that is it will call validateAsNeighbour of its neighbours
	//When achieved a target it returns the target (itself)
	Field *(Field::*validate)(std::vector<Field> * fields);

	//Checking if i found a faster route to the field and all that
	void (Field::*validateAsNeighbour)(Field * potentialPrev, int, int);

	//Stuff to do after it was checked as a neighbour
	void (Field::*afterProcess)();

	//Not done that is the target is not achieved yet, so proceed normally and check all the neighbours
	Field *validateNotDone(std::vector<Field> * fields);

	//This field is a target!
	Field *validateDone(std::vector<Field> * fields);

	//first time i need to determine passability and location
	void validateAsNeighbourFirst(Field * potentialPrev, int x, int y);

	//its passable so i update its stats
	void validateAsNeighbourPassable(Field * potentialPrev, int x, int y);

	//its not passable, or it is already checked so i just end the function immideatly
	void validateAsNeighbourNotPassable(Field * potentialPrev, int x, int y);

	//Its the first time when this field is checked as a neighbour so we need to add it to priority queue
	void notAddedAfterProcess();

	//Nothing to do, return
	void addedAfterProcess();

	//Heuristics used
	double getDistanceToTarget();

public:
	Field(LabData * labirynth) : distance(std::numeric_limits<int>::max()), heur(std::numeric_limits<int>::max()), prev(nullptr), X(-1), Y(-1),
			labirynth(labirynth){
		validate = &Field::validateNotDone;
		validateAsNeighbour = &Field::validateAsNeighbourFirst;
		afterProcess = &Field::notAddedAfterProcess;
	}
	Field(const Field& field) : distance(std::numeric_limits<int>::max()), heur(std::numeric_limits<int>::max()), prev(nullptr), X(-1), Y(-1), labirynth(field.labirynth) {
		validate = &Field::validateNotDone;
		validateAsNeighbour = &Field::validateAsNeighbourFirst;
		afterProcess = &Field::notAddedAfterProcess;
	}

	//a bunch of getters, some inlined
	int getNumber();

	int getX() { return X; }

	int getY() { return Y; }

	double getHeur() { return heur; }

	int getDist() { return distance; }

	Field * prevField() { return prev; }

	//Setters used to calibrade 2 fields into start and target
	//coordinates as arguments are needed because a field dont know its location when its created
	void SetAsStart(int x, int y);
	void SetAsTarget(int x, int y);

	//Check its neighbours
	//Pointer to vector of fields passed as an argument
	Field * Validate(std::vector<Field> * fields) {
		return (this->*Field::validate)(fields);
	};
	//Check it as neigbour, coordinates passed becuase at that time a field might not know its location
	void ValidateNeighbour(Field * potentialPrev, int x, int y) {
		(this->*Field::validateAsNeighbour)(potentialPrev, x, y);
	};
};


#endif //ASTAR_FIELD_H
