//
// Created by wysocki on 18.5.2016.
//

#ifndef ASTARV2_LABDATA_HPP
#define ASTARV2_LABDATA_HPP

class MultithreadPrioQueue;

//Structure for keeping the data about the maze that needs to be accessed by every field to do their jobs
struct LabData {
	const unsigned char *pMap;
	int xSize;
	int ySize;
	int xTarget;
	int yTarget;
	MultithreadPrioQueue * prioQueue;

	LabData(const unsigned char *pMap,
		int xSize, int ySize,
		int xTarget, int yTarget, MultithreadPrioQueue * prioQueue)
		: pMap(pMap), xSize(xSize), ySize(ySize),
		xTarget(xTarget), yTarget(yTarget), prioQueue(prioQueue){

	}
};

#endif //ASTARV2_LABDATA_HPP
