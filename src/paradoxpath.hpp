//
// Created by wysocki on 18.5.2016.
//

#ifndef ASTARV2_PARADOXPATH_HPP
#define ASTARV2_PARADOXPATH_HPP

#include "LabData.hpp"
#include "Field.h"

int FindPath(const int nStartX, const int nStartY,
             const int nTargetX, const int nTargetY,
             const unsigned char *pMap, const int nMapWidth, const int nMapHeight,
             int *pOutBuffer, const int nOutBufferSize);

MultithreadPrioQueue CreateQueue();
void generateRoute(Field * target, int *pOutBuffer, const int nOutBufferSize);

#endif //ASTARV2_PARADOXPATH_HPP
